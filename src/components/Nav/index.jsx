import Link from 'next/link';
import styles from '../../styles/Nav.module.css';

const Nav = (props) => {
  return (
    <nav>
      <Link href={props.url}>
        <div
          className={styles.nav}
          style={{
            backgroundColor: props.color ?? 'dodgerblue',
          }}
        >
          {props.text}
        </div>
      </Link>
    </nav>
  );
};

export default Nav;
