const Cabecalho = ({ title, img }) => {
  // console.log(props);
  return (
    <header>
      <h1>{title && img ? title + img : title + ' não possui img'}</h1>
    </header>
  );
};

export default Cabecalho;
