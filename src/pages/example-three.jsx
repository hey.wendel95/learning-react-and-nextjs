import Link from 'next/link';
import Cabecalho from '../components/Cabecalho';
import Layout from '../components/Layout';
import styles from '../styles/Example.module.css';

const Example = () => {
  return (
    <Layout title="Exemplo One">
      <div>
        <Cabecalho title="Um" />
        <Cabecalho title="Dois" />
        <Cabecalho title="Três" />
      </div>
    </Layout>
  );
};

export default Example;
