import { useState } from 'react';
import Layout from '../components/Layout';

const State = () => {
  const [counter, setCounter] = useState(0);

  return (
    <Layout title="Componente com estado">
      <div>{counter}</div>
      {console.log(counter)}
      <button onClick={() => setCounter((c) => c + 1)}>+</button>
      <button onClick={() => setCounter((c) => c - 1)}>-</button>
    </Layout>
  );
};

export default State;
