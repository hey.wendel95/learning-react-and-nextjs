import Link from 'next/link';
import Layout from '../components/Layout';
import styles from '../styles/ExampleTwo.module.css';

const Dois = () => {
  return (
    <div className={styles.two}>
      <Layout title="Example Two"></Layout>
    </div>
  );
};

export default Dois;
