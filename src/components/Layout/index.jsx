import Link from 'next/link';
import Nav from '../Nav';
import styles from '../../styles/Layout.module.css';

const Layout = (props) => {
  return (
    <div className={styles.layout}>
      <div className={styles.cabecalho}>
        <h1>{props.title ?? 'Mais um exemplo'}</h1>
        <Nav url="/" text="Voltar" />
      </div>

      <div className={styles.conteudo}>{props.children}</div>
    </div>
  );
};

export default Layout;
