import Layout from '../../../components/Layout';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

const ClientByCod = () => {
  const router = useRouter();

  return (
    <Layout title="Navegação dinâmica">
      <div>Unidade = {router.query.unity}</div>
      <div>Código = {router.query.cod}</div>
    </Layout>
  );
};

export default ClientByCod;
