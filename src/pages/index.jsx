import Cabecalho from '../components/Cabecalho';
import Link from 'next/link';
import styles from '../styles/Home.module.css';
import Nav from '../components/Nav';

const Home = () => {
  return (
    <div className={styles.home}>
      <Nav url="/" text="Home" color="red" />
      <Nav url="example" text="Example" />
      <Nav url="example-two" text="Example Two" color="green" />
      <Nav url="/navigation" text="Navigation #01" color="crimson" />
      <Nav url="/client/sp-23/123" text="Navigation #02" color="aquamarine" />
      <Nav url="state" text="State" color="cyan" />
    </div>
  );
};

export default Home;
